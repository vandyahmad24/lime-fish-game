package main

import (
	"fmt"
	"math/rand"
)

type Pole struct {
	Name      string
	Cost      int
	FishLimit string
}

type Bait struct {
	Name     string
	Cost     int
	FishType string
}

type Fish struct {
	Name    string
	Size    string
	MinGold int
	MaxGold int
}

func main() {

	fmt.Println("Lime Test Code Fishing Game by Vandy")

	playerGold := 100

	fishingPoles := []Pole{
		{"Small Fishing Pole", 5, "small"},
		{"Medium Fishing Pole", 10, "medium"},
		{"Big Fishing Pole", 15, "big"},
	}

	baits := []Bait{
		{"Red Bait", 1, "red"},
		{"Blue Bait", 2, "blue"},
		{"Green Bait", 3, "green"},
	}

	chosenPole := getPole(fishingPoles)
	playerGold = playerGold - chosenPole.Cost
	chosenBaits := getBaits(baits, playerGold)

	fmt.Printf("\n Anda memiliki %d gold. Anda Memilih %s untuk memancing, silahkan pilih umpan:\n", playerGold, chosenPole.Name)
	displayBaits(chosenBaits)

	totalGold := 0
	for i := 0; i < 10; i++ {
		totalGold += catchFish(chosenPole, chosenBaits)
	}

	fmt.Printf("\nAnda membayar %d gold hari ini!\n", totalGold)

	if totalGold > 100 {
		fmt.Println("Selamat Lur, Anda Menang!")
	} else if totalGold < 100 {
		fmt.Println("Kalah.")
	} else {
		fmt.Println("Skip hari")
	}
}

func getPole(options []Pole) Pole {
	fmt.Println("\nPilih pancingan")
	for i, option := range options {
		fmt.Printf("%d. %s - Biaya: %d gold\n", i+1, option.Name, option.Cost)
	}

	var choice int
	for {
		fmt.Print("Masukan Nomer yang anda pilih ")
		fmt.Scan(&choice)

		if choice >= 1 && choice <= len(options) {
			return options[choice-1]
		}

		fmt.Println("Invalid nomer. Masukan lagi.")
	}
}

func getBaits(options []Bait, remainingGold int) []Bait {
	var chosenBaits []Bait

	for remainingGold > 0 {
		displayBaits(options)
		fmt.Printf("Sisa gold: %d\n", remainingGold)

		var choice, quantity int
		fmt.Print("Masukan Umpan yang anda pilih (0 untuk keluar): ")
		fmt.Scan(&choice)

		if choice == 0 {
			break
		}

		if choice >= 1 && choice <= len(options) {
			fmt.Print("Berapa yang anda ingin beli?: ")
			fmt.Scan(&quantity)

			bait := options[choice-1]
			totalCost := bait.Cost * quantity

			if totalCost <= remainingGold {
				chosenBaits = append(chosenBaits, Bait{Name: bait.Name, Cost: totalCost, FishType: bait.FishType})
				remainingGold -= totalCost
			} else {
				fmt.Printf("Emas tidak ckup, sisa emas: %d\n", remainingGold)
			}
		} else {
			fmt.Println("Invalid nomer. Masukan lagi.")
		}
	}

	return chosenBaits
}

func displayBaits(baits []Bait) {
	fmt.Println("\nUmpan Tersedia :")
	for i, bait := range baits {
		fmt.Printf("%d. %s - Biaya: %d gold\n", i+1, bait.Name, bait.Cost)
	}
}

func catchFish(pole Pole, baits []Bait) int {
	fishSize := pole.FishLimit

	if len(baits) > 0 {
		baits = baits[1:]
	}

	fishType := getFishType()

	switch fishType {
	case "red":
		fishSize = "small"
	case "blue":
		fishSize = "medium"
	default:
		fishSize = "big"
	}

	gold := calculateFishValue(fishType, fishSize)
	fmt.Printf("Anda Menangkap %s %s ikan dan mendapatkan %d gold!\n", fishType, fishSize, gold)

	return gold
}

func getFishType() string {
	randNum := rand.Intn(100) + 1
	if randNum <= 30 {
		return "red"
	} else if randNum <= 70 {
		return "blue"
	}
	return "green"
}


//get random fish to calculate point
func calculateFishValue(fishType, fishSize string) int {
	fishes := map[string]map[string]Fish{
		"red": {
			"small":  {"Red Small Fish", "small", 1, 5},
			"medium": {"Red Medium Fish", "medium", 5, 10},
			"big":    {"Red Big Fish", "big", 10, 15},
		},
		"blue": {
			"small":  {"Blue Small Fish", "small", 3, 5},
			"medium": {"Blue Medium Fish", "medium", 8, 10},
			"big":    {"Blue Big Fish", "big", 13, 15},
		},
		"green": {
			"small":  {"Green Small Fish", "small", 5, 5},
			"medium": {"Green Medium Fish", "medium", 10, 10},
			"big":    {"Green Big Fish", "big", 15, 15},
		},
	}

	fish := fishes[fishType][fishSize]
	return rand.Intn(fish.MaxGold-fish.MinGold+1) + fish.MinGold
}
